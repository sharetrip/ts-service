package com.share.trip.repository

import com.share.trip.domain.Trip
import org.springframework.data.repository.CrudRepository

interface TripRepository : CrudRepository<Trip, Long> {

    fun findAllByUserId(userId: Long): List<Trip>

    fun findAllByUserIdAndIsGroup(userId: Long, group: Boolean): List<Trip>
}