package com.share.trip.repository

import com.share.trip.domain.City
import org.springframework.data.repository.CrudRepository

interface CityRepository : CrudRepository<City, Long> {
    fun findByName(name: String): City
}