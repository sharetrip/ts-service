package com.share.trip.repository

import com.share.trip.domain.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository: JpaRepository<User, Long> {
    fun findFolksById(userId: Long) : List<User>
}