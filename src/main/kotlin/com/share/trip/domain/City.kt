package com.share.trip.domain

import javax.persistence.*

@Entity
data class City(

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,
        @Column(nullable = false)
        val name: String? = null
)