package com.share.trip.domain

import java.util.*
import javax.persistence.*

@Entity
@SequenceGenerator(name="seq", initialValue=3, allocationSize=100)
data class Trip(
        @Id
        @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
        var id: Long? = null,
        var dateFrom: Date? = null,
        var dateTo: Date? = null,
        var isGroup: Boolean? = false,
        @ManyToOne
        var to: City? = null,
        @ManyToOne
        var from: City? = null,
        @ManyToOne
        var user: User? = null,
        @ManyToMany
        @JoinTable(name = "TRIP_PARTICIPATES",
                joinColumns = arrayOf(JoinColumn(name = "trip_id", referencedColumnName = "id")),
                inverseJoinColumns = arrayOf(JoinColumn(name = "patricipates_id", referencedColumnName = "id")))
        var participates: List<User>? = null) {
}