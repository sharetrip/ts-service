package com.share.trip.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name = "user")
data class User(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long? = null,

        @Column(nullable = false)
        var name: String? = null,

        @Column(nullable = false)
        var telephone: String? = null,

        @ManyToOne
        var city: City? = null,

        @JsonIgnore
        @ManyToMany(fetch = FetchType.LAZY,
                cascade = arrayOf(CascadeType.ALL))
        @JoinTable(name = "folks",
                joinColumns = arrayOf(JoinColumn(name = "user_id", referencedColumnName = "id")),
                inverseJoinColumns = arrayOf(JoinColumn(name = "friend_id", referencedColumnName = "id")))
        var friends: Set<User>? = hashSetOf()

)
