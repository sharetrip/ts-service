package com.share.trip.controller

import com.share.trip.repository.CityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong

@CrossOrigin
@RestController
class MainController {

    @Autowired
    lateinit var cityRepository: CityRepository

    val counter = AtomicLong()

    @GetMapping("/")
    fun greeting(@RequestParam(value = "name", defaultValue = "World") name: String) =
            Greeting(counter.incrementAndGet(), "Hello, $name")

    @GetMapping("/cities")
    fun city() = cityRepository.findAll()
}

data class Greeting(val id: Long, val content: String)