package com.share.trip.controller

import com.share.trip.domain.Trip
import com.share.trip.domain.User
import com.share.trip.service.TripService
import com.share.trip.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@CrossOrigin
@RestController
class UserController {

    val logger = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var tripService: TripService


    @GetMapping("/users")
    fun users() = userService.findAll()

    @PostMapping("/user", consumes = arrayOf("application/json"))
    fun createUser(@RequestBody userRequest: UserRequest) {
        userService.createUser(userRequest)
    }

    @PostMapping("/add/folk", consumes = arrayOf("application/json"))
    fun addFolk(@RequestBody request: AddFolkRequest) {
        userService.addFolk(request)
    }

    @GetMapping("/folks/directions", consumes = arrayOf("application/json"))
    fun getFolksDirections(userId: Long): List<Trip> {
        return userService.getFolksDirections(userId)
    }

    @GetMapping("/friends", consumes = arrayOf("application/json"))
    fun getFriends(): List<User> {
        return userService.getFolks(1)
    }

}

data class UserRequest(
        var name: String,
        var telephone: String,
        var homeTown: Long
)

data class AddFolkRequest(
        var userId: Long? = null,
        var folkId: Long? = null
)