package com.share.trip.controller

import com.share.trip.domain.City
import com.share.trip.domain.Trip
import com.share.trip.domain.User
import com.share.trip.service.TripService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*

@CrossOrigin
@RestController
class TripController {

    val logger = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    lateinit var tripService: TripService

    @GetMapping("/trips")
    fun allTrips(userId: Long): List<Trip> {
        logger.info("Get trips by UserId: $userId")
        return tripService.findAllByUserId(userId)
    }

    @GetMapping("/my_trips")
    fun myTrips(): List<Trip> {
        logger.info("Get trips by UserId: 1")
        return tripService.myTrips(1)
    }

    @GetMapping("/group_trips")
    fun groupTrips(): List<Trip> {
        logger.info("Get trips by UserId: 1")
        return tripService.groupTrips(1)
    }

    @GetMapping("/trip")
    fun trip(id: Long): Trip {
        logger.info("Get trip by id: $id")
        return tripService.findById(id)
    }


    @PostMapping("/create_trip")
    fun createTrip(@RequestBody tripRequest: TripRequest): Boolean {
        logger.info("Create trip: $tripRequest")
        return tripService.createTrip(tripRequest)
    }


    @GetMapping("/trips_intersection")
    fun tripsIntersection(userId: Long): List<TripIntersection> {
        logger.info("Get trips intersection: $userId")
        return tripService.getIntersections(userId)
    }

}

data class TripRequest(
        val from: String,
        val to: String,
        val dateFrom: Date,
        val dateTo: Date,
        val userId: Long,
        val isGroup: Boolean,
        val participates: List<Long>
)

data class TripIntersection(
        val city: City,
        val dateFrom: Date,
        val dateTo: Date,
        val folk: User

)