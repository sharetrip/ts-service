package com.share.trip.service

import com.share.trip.controller.AddFolkRequest
import com.share.trip.controller.UserRequest
import com.share.trip.domain.Trip
import com.share.trip.domain.User
import com.share.trip.repository.CityRepository
import com.share.trip.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestBody


@Service
class UserServiceImpl : UserService {
    @Autowired
    lateinit var cityRepository: CityRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var tripService: TripService

    override fun findAll(): List<User> {
        return userRepository.findAll()
    }

    override fun createUser(@RequestBody userRequest: UserRequest): User {
        val city = cityRepository.findById(userRequest.homeTown)
        val user = User(name = userRequest.name, telephone = userRequest.telephone, city = city.get())
        return userRepository.save(user)
    }

    override fun addFolk(@RequestBody request: AddFolkRequest) {
        val user: User = userRepository.findById(request.userId).get()
        val folk: User = userRepository.findById(request.folkId).get()
        user.friends = user.friends!!.union(hashSetOf(folk))
        userRepository.save(user)
    }

    override fun getFolksDirections(userId: Long): List<Trip> {
        val folks = getFolks(userId)
        return folks.flatMap { folk -> tripService.findAllByUserId(folk.id!!) }
    }

    override fun findById(userId: Long): User {
        return userRepository.findById(userId).get()
    }

    override fun getFolks(userId: Long): List<User> {
        val user = userRepository.findById(userId)
        return user.get().friends!!.flatMap { userRepository.findFolksById(it.id!!) }
    }

}

interface UserService {
    fun createUser(@RequestBody userRequest: UserRequest): User
    fun addFolk(@RequestBody request: AddFolkRequest)
    fun findAll(): List<User>
    fun findById(userId: Long): User
    fun getFolks(userId: Long): List<User>
    fun getFolksDirections(userId: Long): List<Trip>
}