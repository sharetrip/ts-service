package com.share.trip.service

import com.share.trip.controller.TripIntersection
import com.share.trip.controller.TripRequest
import com.share.trip.domain.Trip
import com.share.trip.domain.User
import com.share.trip.repository.CityRepository
import com.share.trip.repository.TripRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class TripServiceImpl : TripService {

    val logger = LoggerFactory.getLogger(this.javaClass)

    @Autowired
    lateinit var tripRepository: TripRepository

    @Autowired
    lateinit var cityRepository: CityRepository

    @Autowired
    lateinit var userService: UserService

    override fun createTrip(tripRequest: TripRequest): Boolean {
        val from = cityRepository.findByName(tripRequest.from)
        val to = cityRepository.findByName(tripRequest.to)
        val user = userService.findById(tripRequest.userId)
        var participates:List<User> =
        tripRequest.participates.map { userService.findById(it)  }
        val trip = Trip(from = from,
                to = to,
                dateFrom = tripRequest.dateFrom,
                dateTo = tripRequest.dateTo,
                user = user,
                isGroup = tripRequest.isGroup,
                participates = participates)
        tripRepository.save(trip)
//        logger.info("Saved trip: $save")
        return true
    }

    override fun myTrips(userId: Long): List<Trip> =
            tripRepository.findAllByUserIdAndIsGroup(userId = userId, group = false)

    override fun groupTrips(userId: Long): List<Trip> =
            tripRepository.findAllByUserIdAndIsGroup(userId = userId, group = true)


    override fun findById(userId: Long) = tripRepository.findById(userId).get()

    override fun findAllByUserId(userId: Long): List<Trip> =
            tripRepository.findAllByUserId(userId)

    override fun getIntersections(userId: Long): List<TripIntersection> {
        val user = userService.findById(userId)
        val folks1 = user.friends
        val folks = userService.getFolks(userId)

        folks.stream().map { }
        return emptyList()
    }

}

interface TripService {
    fun findAllByUserId(userId: Long): List<Trip>
    fun myTrips(userId: Long): List<Trip>
    fun groupTrips(userId: Long): List<Trip>
    fun findById(userId: Long): Trip
    fun createTrip(tripRequest: TripRequest): Boolean
    fun getIntersections(userId: Long): List<TripIntersection>
}