insert into city
values (1, 'Kyiv');
insert into city
values (2, 'Berlin');
insert into city
values (3, 'Lisbon');
insert into city
values (4, 'Barselona');

insert into `user`
values (1, 'Bob', '+4489053409', 2);
insert into `user`
values (2, 'Jimmy', '+380123786478', 1);
insert into `user`
values (3, 'Kitty', '+490123786478', 3);

insert into folks values (1, 2);
insert into folks values (1, 3);

--
insert into trip values (1,'2018-09-28','2018-10-04', true, 1, 2, 1);
insert into trip values (2, '2018-10-04', '2018-10-08', false, 2, 3, 1);
insert into trip values (3, '2018-10-08', '2018-10-13', false, 3, 4, 1);

insert into TRIP_PARTICIPATES  values (1, 1);
insert into TRIP_PARTICIPATES  values (1, 2);
insert into TRIP_PARTICIPATES  values (1, 3);
